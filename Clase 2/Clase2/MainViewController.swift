//
//  ClaseCocoViewController.swift
//  Clase2
//
//  Created by Jose Azadobay on 19/10/18.
//  Copyright © 2018 Jose Azadobay. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ClaseCocoViewController: UIViewController {

    
    let game = Game()
    
    @IBOutlet weak var score: UILabel!
    
    @IBOutlet weak var roundLabel: UILabel!
    
    @IBOutlet weak var hit: UILabel!
    
    @IBOutlet weak var numeroActual: UILabel!
    
    @IBOutlet weak var SliderOutlet: UISlider!
    
    
    
    var ref:DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        //setValue();
        //score.text = "Score : \(game.score)"
        //round.text = "Round: \(game.roundCount)"
        //hit.text = "Hit:\(game.numberToHit)"
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func gameSlide(_ sender: Any) { //El sender aqui es un slider
        
        let slider = sender as! UISlider
        let sliderValue = Int(round(slider.value))
        
        game.play(sliderValue: sliderValue)
        numeroActual.text = "Your guess was: \(sliderValue)"

        //score.text = "Score : \(game.score)"
        //roundLabel.text = "Round: \(game.roundCount)"
        //hit.text = "Hit:\(game.numberToHit)"
        setValue();
        print(slider.value)
        
        
        if game.roundCount == 3 {
            slider.isEnabled = false
            sendScore()
            numeroActual.text = "Final Score: \(game.score)"
        }
    }
    
   
    //outlet: solo una conexion
    //outlet connection: varias referencias.
    

    @IBAction func restart(_ sender: Any) { // El sender aqui es un boton
        
        game.restart()
        SliderOutlet.isEnabled = true
        
        setValue();
        
        //hit.text = "Hit:\(game.numberToHit)"
        //score.text = "Score : \(game.score)"
        //roundLabel.text = "Round: \(game.roundCount)"
        //hit.text = "Hit:\(game.numberToHit)"
        
        
    }
    
    func setValue(){
        score.text = "Score : \(game.score)"
        roundLabel.text = "Round: \(game.roundCount)"
        hit.text = "Hit:\(game.numberToHit)"
    }
    
    @IBAction func scoreButtonPressed(_ sender: Any) {
        
    self.performSegue(withIdentifier: "scoreSegue", sender: self)
        
    }
    
    func sendScore(){
        let usuario = "Steven"
        let finalScore = game.score
        
        let uuid = UUID().uuidString
        self.ref.child("score").child(uuid).setValue([
            "user": usuario ?? "n/a",
            "finalscore": String(finalScore) ?? ""
            ])
    }
    
}
