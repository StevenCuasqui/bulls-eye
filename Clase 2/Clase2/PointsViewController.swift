//
//  PointsViewController.swift
//  Clase2
//
//  Created by Steven Cuasqui on 12/14/18.
//  Copyright © 2018 Jose Azadobay. All rights reserved.
//

import UIKit
import FirebaseDatabase

class PointsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var scoresTableView: UITableView!
    var ref:DatabaseReference!
    var scores:[[String:String]] = []  //Es un arreglo de diccionarios que se inicializa en vacio
    var selectedIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        getFireBaseinfo()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func returnButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func getFireBaseinfo(){
        let scoreRef = self.ref.child("score")
        scoreRef.observe(.childAdded){(dataSnapshot) in  //escucha lo que se va añadiendo (.childAdded)
            //print(dataSnapshot)
            //as realiza casteos
            var scoreDict = dataSnapshot.value as? [String:String] ?? [:]
            //obtener el id
            scoreDict["id"] = dataSnapshot.key
            
            self.scores += [scoreDict]
            print(self.scores)
            self.scoresTableView.reloadData() //La tabla es refrescada para mostrar ahora sì el contenido de la tabla (Para consultas asincronas y API's)
        }
        
        //Eliminar el item, la funcion de reloadData se elimina para optimizar y se lo ubica en el willAppear. Se corrige manteniendo esta funcion pero el getFireBaseinfo va en el willAppear.
        scoreRef.observe(.childRemoved){(dataSnapshot) in
            let id = dataSnapshot.key
            self.scores =   self.scores.filter(){ $0["id"] != id}
            self.scoresTableView.reloadData()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
         //cell.textLabel?.text = "\(indexPath)"
         let username = scores[indexPath.row]["user"] ?? "user"
         let score = scores[indexPath.row]["finalscore"] ?? "finalscore"
         cell.textLabel?.text = "\(username) - \(score)"
         return cell
        
    }
    
    //Funcion para cambiar el indice que es seleccionado en la tabla
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedIndex = indexPath.row
        return indexPath
    }
    
    
}
